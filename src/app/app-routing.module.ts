import {Index} from './pages/index/component';
import {Board} from './pages/board/component';
import { NgModule }     from '@angular/core';
import {
  RouterModule, Routes,
} from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    component: Index,
    pathMatch: 'full'
  },
  {
    path: 'board',
    component: Board,
    pathMatch: 'full'
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule {}