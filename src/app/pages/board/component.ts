
import {Component, OnInit} from '@angular/core'
import {ActivatedRoute, Params} from '@angular/router'
@Component({
    selector:'fg-Board',
    templateUrl:'./component.html',
    styleUrls: ['./component.css']
})
export class Board implements OnInit{
    constructor(private route: ActivatedRoute){}

    page: number;
    boardid: number;
    ngOnInit(){
        console.log(this.route.snapshot);
        this.page = this.route.snapshot.params['page'];
        this.boardid = this.route.snapshot.params['id'];
    }

    click(){
        
    }

}