import {
    applyRedirects
} from 'c:/Users/Station1/Documents/ForoGeomMain/node_modules/@angular/router/src/apply_redirects';
import * as http from 'http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng'
import { BarHeaderComponent } from './bars/header/component'
import { BarNavigationComponent } from './bars/navigation/component'
import { BarCategoryComponent } from './bars/category/component'
import { BarBoardComponent } from './bars/board/component'
import {BarTopicListComponent} from './bars/topiclist/component'
import { ApiModule } from './services/api/api.module'
import { AppRoutingModule } from './app-routing.module'
import { Index } from './pages/index/component'
import { Board } from './pages/board/component'
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    InputTextModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    ApiModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    BarHeaderComponent,
    BarNavigationComponent,
    BarCategoryComponent,
    BarBoardComponent,
    BarTopicListComponent,
    Index,
    Board
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
