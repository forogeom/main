import {Component, OnInit} from '@angular/core'
import {APIService} from '../../services/api/apiMain';


@Component({
    selector:'fg-bar-header',
    templateUrl:'./component.html',
    styleUrls: ['./component.css']
})
export class BarHeaderComponent implements OnInit{
    
    header: Object = {
        Title: "ForoGeom",
        Subtitle: "Where Forum meets Angular 2",
        Webmaster: "brandynabel@gmail.com",
        ID: 1
    };
    constructor(private api: APIService){}
    ngOnInit(){
        this.api.getResult('system/get').subscribe(data => {
            this.header = data[0];
            console.log(data)
        });
    }

}