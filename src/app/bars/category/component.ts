import {ApiModule} from '../../services/api/api.module';
import {APIService} from '../../services/api/apiMain';
import { Component, OnInit} from '@angular/core'
import { Category } from '../../models/category';
@Component({
    selector:'fg-bar-category',
    templateUrl:'./component.html',
    styleUrls: ['./component.css']
})
export class BarCategoryComponent implements OnInit{
    categoryList: Array<Category>;
    res: any;
    constructor(private api: APIService ){}
    ngOnInit(){

            this.api.getResult('category/get/list').subscribe(data => {
            this.categoryList = data;
            console.log(data)
        });

        
        
    }

}