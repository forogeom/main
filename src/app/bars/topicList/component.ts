import {Router} from '@angular/router'
import { Component, OnInit, Input } from '@angular/core'
import { Board } from '../../models/board';
import {APIService} from '../../services/api/apiMain'
@Component({
    selector: 'fg-bar-topicList',
    templateUrl: './component.html',
    styleUrls: ['./component.css']
})
export class BarTopicListComponent implements OnInit {
    @Input() boardid: string;
    topicList: Array<Object>;
    res: any;
    constructor(private api: APIService, private router:Router) { }
    ngOnInit() {

        console.log('catid:' + this.boardid);
        this.api.getResult('topic/get/list/board/?board=' + this.boardid).subscribe(data => {
            this.topicList = data;
            console.log(data)
        });
    }

    click(boardid: number) {
        console.log(boardid);
        this.router.navigate(['/board', {id: boardid, page: 1}]);
    }
}