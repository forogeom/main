import {
    routerNgProbeToken
} from 'c:/Users/Station1/Documents/ForoGeomMain/node_modules/@angular/router/src/router_module';
import {Router} from '@angular/router'
import { Component, OnInit, Input } from '@angular/core'
import { Board } from '../../models/board';
import {APIService} from '../../services/api/apiMain'
@Component({
    selector: 'fg-bar-board',
    templateUrl: './component.html',
    styleUrls: ['./component.css']
})
export class BarBoardComponent implements OnInit {
    @Input() catid: string;
    boardList: Array<Board>;
    res: any;
    constructor(private api: APIService, private router:Router) { }
    ngOnInit() {

        console.log('catid:' + this.catid);
        this.api.getResult('board/get/list/category/?category=' + this.catid).subscribe(data => {
            this.boardList = data;
            console.log(data)
        });
    }

    click(boardid: number) {
        console.log(boardid);
        this.router.navigate(['/board', {id: boardid, page: 1}]);
    }
}