import { NgModule } from '@angular/core';
import { APIService } from './apiMain';
import {HttpModule} from '@angular/http'
 @NgModule({
    imports: [
        HttpModule
    ],
    declarations: [

    ],
    providers: [
        APIService
    ],
    exports: [

    ]
})
export class ApiModule { }
